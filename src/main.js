import { createApp } from 'vue'

import App from './App.vue'

import BaseCard from './components/ui/BaseCard.vue'
import BaseButton from './components/ui/BaseButton.vue'
import BaseWrapper from './components/ui/BaseWrapper.vue'

import router from './router'

const app = createApp(App)

app.use(router)

app.component('base-card', BaseCard)
app.component('base-button', BaseButton)
app.component('base-wrapper', BaseWrapper)

app.mount('#app')
