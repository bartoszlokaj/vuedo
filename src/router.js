import {createRouter, createWebHistory} from 'vue-router'

import HomePage from './pages/HomePage.vue';
import ContactPage   from './pages/ContactPage.vue';
import NotFound from './pages/NotFound.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', component: HomePage},
        {path: '/contact', component: ContactPage},
        {path: '/:notFoud(.*)', component: NotFound}
    ]
})

export default router;